import  * as Socket from 'socket.io';
import  * as Express from "express";
import { v4 as uuidv4 } from 'uuid';
import { Pad } from './Pad';
import * as bodyParser from 'body-parser';

require('dotenv').config()

const app: Express.Express = require('express')();
const http: any = require('http').Server(app);
const io: any = require('socket.io')(http);
const cors = require('cors');
const port: Number = 4000;
const pads:Pad[] =[];

// TODO: load this from environment
var corsOptions = {
    origin: process.env.CLIENT_HOST,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
};

console.log("testing dotenv, ", process.env.TEST_ENV);

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded());
app.use(bodyParser.json())

// REST API
app.post('/pads', function (req: Express.Request, res: Express.Response) {
    if (req.body['content'] == null) {
        res.status(400).send();
    } else {
        const id: string = uuidv4();
        const pad: Pad = {id: id, content: req.body['content']};
        pads.push(pad);
        res.setHeader('Content-Type', 'application/json');
        res.send(pad);
    }
 });

app.get('/pads/:id', function(req: Express.Request, res: Express.Response) {
    const id: string = req.params['id'];
    const pad: Pad = pads.find(currentPad => currentPad.id == id)
    if (pad) {
        res.setHeader('Content-Type', 'application/json');
        res.send(pad);
    } else {
        return res.sendStatus(404);
    }
});

// SOCKET API
io.on('connection', (socket: Socket.Socket) => initializeSocketConnection(socket));

 const initializeSocketConnection = (socket: Socket.Socket) => {
    
    let previousId: string;

    socket.on('padUpdateFromClient', (updatedPad: Pad) => {
        const padToBeUpdated = pads.find(pad => pad.id == updatedPad.id);
        const indexToBeUpdated = pads.indexOf(padToBeUpdated);
        pads[indexToBeUpdated] = updatedPad;
        socket.to(pads[indexToBeUpdated].id).emit('padUpdateFromServer', updatedPad);
    });

    socket.on('joinRequestFromClient', (padId: string) => {
        socket.leave(previousId);
        socket.join(padId);
        previousId = padId;
        const pad = pads.find(pad => pad.id == padId)
        socket.emit('padUpdateFromServer', pad);
    });
}

http.listen(port);
