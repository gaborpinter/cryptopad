import { TestBed } from '@angular/core/testing';

import { PadSocketService } from './pad-socket.service';

describe('PadSocketService', () => {
  let service: PadSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PadSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
