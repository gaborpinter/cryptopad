import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pad } from './pad';
import { map } from 'rxjs/operators';
import * as rxjs from 'rxjs';
import { CryptoService } from './crypto.service';

// TODO: extract this to .env
const resourceUrl: string = 'http://localhost:4000/pads';

@Injectable({
  providedIn: 'root'
})
export class PadService {

  constructor(
    private http: HttpClient,
    private cryptoService: CryptoService
  ) { }

  createPadAndGetPadPath(): rxjs.Observable<string> {
    const secret: string = this.cryptoService.createSecret();
    const encryptedContent: string = this.cryptoService.encrypt('This is a secret, encrypted note. Feel free to share the URL with someone!', secret);

    return this.http
      .post(resourceUrl, {content: encryptedContent})
      .pipe(map((pad: Pad) => `/pads/${pad.id}?secret=${secret}`));
  }

  getPadEncrypted(id: string): rxjs.Observable<any> {
    return this.http.get(`${resourceUrl}/${id}`);
  }

  getPadDecrypted(id: string, secret: string): rxjs.Observable<any> {
    return this.http
      .get(`${resourceUrl}/${id}`)
      .pipe(map((pad: Pad) => {
        return {
          id: pad.id,
          content: this.cryptoService.decrypt(pad.content, secret)
        }  
      }));
  }
}
