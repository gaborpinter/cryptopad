import { Injectable } from '@angular/core';
import { Pad } from './pad'
import * as Socket  from 'ngx-socket-io';
import * as rxjs from 'rxjs';

// TODO: separate components, modules, services and models
@Injectable({
  providedIn: 'root'
})
export class PadSocketService {

  public pad: rxjs.Observable<Pad> = this.socket.fromEvent<Pad>('padUpdateFromServer');

  constructor(private socket: Socket.Socket) {  }

  joinPad(id: string): rxjs.Observable<Pad> {
    return this.socket.emit('joinRequestFromClient', id);
  }

  updatePad(encryptedPad: Pad): rxjs.Observable<Pad> {
    return this.socket.emit('padUpdateFromClient', encryptedPad);
  }
}
