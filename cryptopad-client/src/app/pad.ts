export interface Pad {
    id: string;
    content: string;
}