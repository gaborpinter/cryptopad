import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/home/home.component'; 
import { PadComponent } from 'src/app/pad/pad.component'; 

const routes: Routes = [
    {path: '', component: HomeComponent }, 
    {path: 'pads/:id', component: PadComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
export class AppRoutingModule { }