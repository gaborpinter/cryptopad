import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PadSocketService } from '../pad-socket.service';
import * as rxjs from 'rxjs';
import { startWith } from 'rxjs/operators';
import { Pad } from '../pad';
import { map } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
import { CryptoService } from '../crypto.service';
import { PadService } from '../pad.service';
import { State } from '../state';

@Component({
  selector: 'app-pad',
  templateUrl: './pad.component.html',
  styleUrls: ['./pad.component.scss']
})
export class PadComponent implements OnInit, OnDestroy {

  public pad: Pad;
  private subscription: rxjs.Subscription;
  public url: string;
  public buttonText: string;
  private secret: string;
  private state: State;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cryptoService: CryptoService,
    private padSocketService: PadSocketService,
    private padService: PadService
  ) { }

  ngOnInit(): void {
    this.state = State.loading;
    this.url = window.location.href;
    
    const id = this.route.snapshot.params['id'];
    this.secret = this.router.parseUrl(this.router.url).queryParams['secret'] || '';

    // TODO: check if decryption is proper
    this.padService.getPadDecrypted(id, this.secret)
      .subscribe((pad: Pad) => {
        this.padSocketService.joinPad(id);
        this.subscribeToChangesInPad(id);
        this.state = State.done;
      }, (error: any) => {
        this.state = State.error;
      });
  }

  private subscribeToChangesInPad(id: string) {
    this.subscription = this.padSocketService.pad
      .pipe(startWith({id: id, content: ''}))
      .subscribe((pad: Pad) => {
        this.pad = {
          id: pad.id,
          content: this.cryptoService.decrypt(pad.content, this.secret)
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  updateContent() {
    this.padSocketService.updatePad({
      id: this.pad.id,
      content: this.cryptoService.encrypt(this.pad.content, this.secret)
    });
  }

  isStateLoading() { return this.state === State.loading; }
  isStateDone() { return this.state === State.done; }
  isStateError() { return this.state === State.error; }
}
