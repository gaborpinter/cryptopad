import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor() { }

  public decrypt(content: string, secret: string): string {
    var decrypted: CryptoJS.DecryptedMessage = CryptoJS.AES.decrypt(content, secret, {
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.NoPadding
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  public encrypt(content: string, secret: string): string {
    return CryptoJS.AES.encrypt(content, secret, {
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.NoPadding
    }).toString();
  }

  public createSecret(): string {
    return CryptoJS.lib.WordArray.random(128 / 8).toString();
  }
}
